﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace StudentManager.Services
{
    public interface IDbFactory
    {
        IMongoDatabase Database { get; }
    }
}
