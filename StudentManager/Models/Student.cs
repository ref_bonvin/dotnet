using System.ComponentModel.DataAnnotations;
using System;

namespace StudentManager.Models
{
    public class Student : StudentEntity
    {

    [Required]
    [MaxLength(20)]
    public string LastName {get; set; }

    [Required]
    [MaxLength(20)]
    public string FistName {get; set; }

    [Required]
    [DataType(DataType.Date)]
    public DateTime BirthDate {get; set; }


    [Required]
    [EmailAddress]
    public string Email {get; set; }

        public string ReturnMessage()
        {
            return "Happy coding!";
        }
    }
}
